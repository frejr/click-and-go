void I2C_Receive(int bytes)
{
  //=============================== Otrzymujemy komende I2C od mastera lub pierwszego MCU ===============================
  for(byte i = 0; i < bytes; i++)
  {
    datapack[i] = Wire.read();        // Zapisujemy dane z magistrali do listy
  }

  command = datapack[0];              // Pierwsza otrzymana dana to rodzaj komendy do wykonania
  packsize = bytes;                   // Liczba przeslanych przez I2C bajtow
}
