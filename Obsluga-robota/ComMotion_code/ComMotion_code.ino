#include <EEPROM.h>
#include <Wire.h>
#include "IOpins.h"

byte mcu;                                                                                               // Zmienna wskazujaca na ktorym MCU jestesmy
byte address;                                                                                           // Adres aktualnego MCU (MCU 1 = 30, MCU 2 = 31)

//=============================== Zmienne dotyczace wlasciwosci silnikow i enkoderow ===============================

int acount, bcount;                                                                                     // Zmienne zliczajace enkoder A i B
volatile byte aflag;                                                                                    // Flaga zaciecia sie silnika A
volatile byte bflag;                                                                                    // Flaga zaciecia sie silnika B
volatile unsigned long apulse, bpulse;                                                                  // Pulsacje enkoderow
volatile unsigned long atime, btime;                                                                    // Aktualny czas zmiany stanu silnikow
byte motora, motorb;                                                                                    // Zmienna wskazujaca ktorym z silnikow sie zajmujemy
long maxpulse[4];                                                                                       // Maksymalny czas pulsacji enkoderow

//=============================== Zmienne konfiguracyjne ==========================================================

byte lowbat = 60;                                                                                       // Zmienna niskiego poziomu baterii (60 = 6.0V)
int maxamps[4];                                                                                         // Maksymalne natezenie (255 = 2.55A) kazdego z silnikow
byte master = 1;                                                                                        // Adres mastera w komunikacji I2C
byte addroffset = 0;                                                                                    // I2C address offset
byte defaulted;                                                                                         // Sprawdzamy czy zmienne standardowe są załadowane, standardowa konfiguracja zapisywana jest w pamięci EEPROM
byte encoders;                                                                                          // Flaga enkoderow (1 - enkodery uzywane)
int motrpm[4];                                                                                          // RPM silnikow uzywane do obliczenia maksymalnej predkosci
int encres[4];                                                                                          // Rozdzielczosc enkoderow (x100)
byte reserve[4];                                                                                        // Zachowanie mocy rezerwowej dla poprawnego dzialania silnikow
long stalltm[4];                                                                                        // Czas zaciecia sie silnikow, uzywany do sprawdzenia czy silniki poruszaja sie wolno czy sa zaciete

//=============================== Zmienne kontrolujace motor shielda ==============================================

int velocity = 0;                                                                                       // Predkosc wypadkowa robota
int angle = 0;                                                                                          // Kat skretu robota
int rotation = 0;                                                                                       // Predkosc obrotu robota
int mspeed[4];                                                                                          // Predkosc kazdego z silnikow

float radconvert = PI/180;                                                                              // Zmienna konwertujaca stopnie do radianow

byte analogpin[5] = {0, 1, 3, 6, 7};                                                                    // Piny analogowe (A0 - natezenie silnika A, A1 - natezenie silnika B, A3 & A6 - zapasowe, A7 - stan napiecia baterii na MCU 1 lub zapas na MCU 2
int analogvalue[5];                                                                                     // Stany pinow analogowych
byte datapack[32];                                                                                      // Paczka danych otrzymana przez I2C
byte sendpack[32];                                                                                      // Paczka danych do wyslania przez I2C
byte spsize = 0;                                                                                        // Rozmiar wysyłanej przez nas paczki danych
byte syncpack = 1;
byte packsize;                                                                                          // Rozmiar przesylanej paczki danych
byte command = 255;                                                                                     // Komenda otrzymana przez I2C, command = datapack[0], command = 255 => nie ma komendy
byte analog;                                                                                            // Sczytujemy rozne piny analogowe w kazdej petli (sczytanie analogowej wartosci zajmuje 260us)
byte powerdown = 0;                                                                                     // Flaga wylaczajaca wszystkie silniki w razie niskiego stanu baterii

int voltage;                                                                                            // Napiecie baterii
byte eflag = 0;                                                                                         // Flaga bledow, kazdy z 8 bitow wskazuje jakis blad zwiazany z silnikami lub niskim poziomem baterii
byte i2cfreq = 1;                                                                                       // Wartosc czestotliwosci taktowania komunikacji I2C ustawiona na standardowa

void setup() {
  EEPROMdefaults();
  EEPROMload();

  for(byte i=0; i<4; i++)
  {
    maxpulse[i] = 60000000L/(long(motrpm[i])*long(encres[i])/100L)*255L;                                 // uS na minute / (RPM * rozdzielczosc enkoderow na RPM) * 255
    maxpulse[i] = maxpulse[i]*(100L-long(reserve[i]))/100L;                                              // Zmniejszone o procent rezerwy
  }

  DDRD = B00000011;                                                                                      // Dipswitche i enkodery sa ustawione na input
  PORTD = PORTD|B11111100;                                                                               // Wlaczamy rezystory na dipswitchach i enkoderach
  DDRB = DDRB|B00001111;                                                                                 // Ustawiamy piny D8-D11 na kontrole silnikow

  mcu = digitalRead(IDpin);                                                                              // Stan niski = MCU 1  stan wysoki = MCU 2

  address = ((PIND&B11110000)>>3)+addroffset+mcu;                                                        // Sprawdzamy adres obecnego procesora, zalezne od ustawienie dip switchow na motor shieldzie, standardowo adres MCU 1 = 30
  motora = mcu*2;
  motorb = mcu*2+1;

  digitalWrite(18, 1);                                                                                   // Wlaczamy rezystor dla pina SDA - linia szeregowa komunikacji I2C
  digitalWrite(19, 1);                                                                                   // Wlaczamy rezystor dla pina SCL - zegar szeregowy komunikacji I2C

  Wire.begin(address);                                                                                   // Rozpoczecie komunikacji I2C oraz ustawienie adresu slave danego procesora
  Wire.onReceive(I2C_Receive);                                                                           // Funkcja wykonywana jesli master chce nam wyslac jakies dane
  Wire.onRequest(I2C_Send);                                                                              // Funkcja wykonywana jesli master chce od nas uzyskac jakies dane
  Wire.setTimeout(5L);                                                                                   // 5 sekundowy timeout dla I2C

  if(i2cfreq == 0)
  {
    TWBR = 72;                                                                                           // Ustawiamy czestotliwosc zegara I2C na 100kHz
  }
  else
  {
    TWBR = 12;                                                                                           // Mozemy zmienic czestotliwosc zegara I2C na 400kHz
  }

  delay(10);

  attachInterrupt(digitalPinToInterrupt(encapin), Aencoder, CHANGE);
  attachInterrupt(digitalPinToInterrupt(encbpin), Bencoder, CHANGE);
}

void loop() {
  analog++;                                                                                              // Za kazdym razem zczytujemy inna z 5 wartosci
  if(analog>4) analog = 0;                                                                               // natezenie A, natezenie B, A3, A6, A7

  analogvalue[analog] = analogRead(analogpin[analog]);                                                   // Sczytujemy wartosci analogowe i zapisujemy je w tablicy do interpretacji
  if(mcu == 0 && analog == 4) voltage = analogvalue[4]*30/185;                                            // Obliczamy wartosc napiecia baterii

  //=============================== Jesli bateria jest slaba wylaczamy silniki ===============================
  
  if(mcu == 0 && analog == 4 && powerdown < 250)                                                         // Sczytalismy wartosc baterii i nie wykryto jeszcze powerdown
  {
    if(analogvalue[4] <= lowbat)                                                                         // Jesli aktualna wartosc baterii jest mniejsza od progowej
    {
      eflag = eflag|B00010000;                                                                           // Piaty bit zglasza spadek mocy ponizej progowej wartosci
      powerdown++;                                                                                       // Zwiekszamy licznik niedoboru mocy
    }
    else
    {
      powerdown = 0;                                                                                     // Resetujemy licznik niedoboru mocy jesli bateria jest jednak naladowana
    }
    if(powerdown > 249) PowerDown();                                                                     // Jesli bateria miala niski poziom napiecia przez 250 cykli wylaczamy silniki
  }
  if(powerdown > 249) return;                                                                            // Jesli mamy niedobor mocy musimy wymienic baterie lub zresetowac program

  //=============================== Funkcje motor shielda =====================================================

  if(command < 32) Commands();                                                                           // Wykonujemy okreslone instrukcje w zaleznosci od komendy otrzymanej przez magistrale I2C
  if(encoders)                                                                                           // Jesli enkodery sa uaktywnione przechodzimy do funkcji obslugujacej silniki
  {
    Motors();
  }
  else                                                                                                   // W przeciwnym razie przesylamy wartosci bezposrednio do PWM
  {
    analogWrite(pwmapin, abs(mspeed[mcu*2]));                                                            // Predkosc silnika A
    digitalWrite(dirapin, mspeed[mcu*2] > 0);                                                            // Kierunek obrotu silnika A
    analogWrite(pwmbpin, abs(mspeed[mcu*2+1]));                                                          // Predkosc silnika B
    digitalWrite(dirbpin, mspeed[mcu*2+1] > 0);                                                          // Kierunek obrotu silnika B
  }
}
