void Motors()
{
  //=============================== Obsluga pulsacji silnikow zaleznie od enkoderow ===============================
  unsigned long actual;                                                            // Chwilowa pozadana predkosc
  static byte apwm, bpwm;                                                          // Predkosc silnikow A i B
  static byte astall, bstall;                                                      // Flagi wskazujace czy silniki sa zaciete

  if(aflag == 1 || astall == 1)                                                    // Jesli enkoder A zmienil polozenie lub silnik sie zacial 
  {
    apulse = (micros() - atime);                                                   // Jak długo uplynelo od ostatniej zmiany stanu
    atime = micros();                                                              // Aktualizujemy czas zmiany stanu silnika

    if(aflag) acount = acount + (mspeed[motora] > 0) - (mspeed[motora] < 0);       // Zliczamy ile razy enkoder A zmienił swoj stan
    actual = maxpulse[motora]/abs(mspeed[motora]);                                 // Obliczamy pozadany czas miedzy pulsacjami enkodera
    if(actual > apulse && apwm > 0) apwm--;                                        // Jesli silnik za szybko, zmniejszamy puls
    if(actual < apulse && apwm < 150) apwm++;                                      // Jesli silnik za wolno, zwiekszamy puls
    if(mspeed[motora] == 0) apwm = 0;                                              // Jesli silnik nie chodzi, wylaczamy puls
    analogWrite(pwmapin, apwm);                                                    // Wysylamy puls na pin PWM
    digitalWrite(dirapin, mspeed[motora] > 0);                                     // Ustalamy kierunek silnika na pin
    astall = 0;                                                                    // Resetujemy flage zaciecia silnika
    aflag = 0;                                                                     // Resetujemy flage enkodera
  }

  if(bflag == 1 || bstall == 1)                                                    // Jesli enkoder B zmienil polozenie lub silnik sie zacial 
  {
    bpulse = (micros() - btime);                                                   // Jak długo uplynelo od ostatniej zmiany stanu
    btime = micros();                                                              // Aktualizujemy czas zmiany stanu silnika

    if(bflag) bcount = bcount + (mspeed[motorb] > 0) - (mspeed[motorb] < 0);       // Zliczamy ile razy enkoder B zmienił swoj stan
    actual = maxpulse[motorb]/abs(mspeed[motorb]);                                 // Obliczamy pozadany czas miedzy pulsacjami enkodera
    if(actual > bpulse && bpwm > 0) bpwm--;                                        // Jesli silnik za szybko, zmniejszamy puls
    if(actual < bpulse && bpwm < 150) bpwm++;                                      // Jesli silnik za wolno, zwiekszamy puls
    if(mspeed[motorb] == 0) bpwm = 0;                                              // Jesli silnik nie chodzi, wylaczamy puls
    analogWrite(pwmbpin, bpwm);                                                    // Wysylamy puls na pin PWM
    digitalWrite(dirbpin, mspeed[motorb] > 0);                                     // Ustalamy kierunek silnika na pin
    bstall = 0;                                                                    // Resetujemy flage zaciecia silnika
    bflag = 0;                                                                     // Resetujemy flage enkodera
  }

  //=============================== Sprawdzamy czy zaden z silnikow sie nie zacial ===============================

  if(analogvalue[0] > maxamps[motora])                                             // Jesli natezenie pradu na silniku przekroczy wartosc maksymalna
  {
    apwm = apwm/2;                                                                 // Zmniejszamy pulsacje silnika o polowe
    if(mcu == 0) eflag = eflag|B00000001;                                          // Jesli MCU 1 wlaczamy flage bledu silnika pierwszego
    if(mcu == 1) eflag = eflag|B00000100;                                          // Jesli MCU 2 wlaczamy flage bledu silnika trzeciego
  }
  if(mspeed[motora] == 0)                                                          // Jesli predkosc silnika A ma byc zerowa
  {
    apwm = 0;                                                                      // Upewniamy sie ze pulsacja jest zerowa
    analogWrite(pwmapin, apwm);                                                    // Wysylamy wartosc pulsacji na pin PWM
  }
  else
  {
    if(micros() - atime > (stalltm[motora]*1000L))                                 // Jesli enkoder nie zmienil swojego polozenia przez 10ms
    {
      astall = 1;                                                                  // Wlaczamy flage zaciecia silnika
      apwm += 2;                                                                   // Zwiekszamy pulsacje pina PWM
      if(apwm > 253) apwm = 253;
    }
  }

  if(analogvalue[1] > maxamps[motorb])                                             // Jesli natezenie pradu na silniku przekroczy wartosc maksymalna
  {
    bpwm = bpwm/2;                                                                 // Zmniejszamy pulsacje silnika o polowe
    if(mcu == 0) eflag = eflag|B00000010;                                          // Jesli MCU 1 wlaczamy flage bledu silnika drugiego
    if(mcu == 1) eflag = eflag|B00001000;                                          // Jesli MCU 2 wlaczamy flage bledu silnika czwartego
  }
  if(mspeed[motorb] == 0)                                                          // Jesli predkosc silnika B ma byc zerowa
  {
    bpwm = 0;                                                                      // Upewniamy sie ze pulsacja jest zerowa
    analogWrite(pwmbpin, bpwm);                                                    // Wysylamy wartosc pulsacji na pin PWM
  }
  else
  {
    if(micros() - btime > (stalltm[motorb]*1000L))                                 // Jesli enkoder nie zmienil swojego polozenia przez 10ms
    {
      bstall = 1;                                                                  // Wlaczamy flage zaciecia silnika
      bpwm += 2;                                                                   // Zwiekszamy pulsacje pina PWM
      if(bpwm > 253) bpwm = 253;
    }
  }
}

//=============================== Funkcje obslugi przerwan dla enkoderow ===============================

void Aencoder()                                                                    // Funkcja przerwania enkodera A
{
  aflag = 1;                                                                       // Ustawiamy flage enkodera A
}

void Bencoder()                                                                    // Funkcja przerwania enkodera B
{
  bflag = 1;                                                                       // Ustawiamy flage enkodera B
}
