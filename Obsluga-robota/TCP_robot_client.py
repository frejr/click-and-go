#!/pi/bin/env python
# -*- coding: utf-8 -*-
import socket
import select
import time
from threading import Thread
import serial


IP = '192.168.0.38'
PORT = 5000

BAUDRATE = 9600
USB_PORT = "/dev/ttyACM0"


class ClientThread(Thread):
    def __init__(self, ip, port):
        Thread.__init__(self)
        self.server_ip = ip
        self.server_port = port
        self.client_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.connected = False
        self.is_running = True
        self.robot_velocity = 0
        self.robot_angle = 0
        self.robot_rotation = 0

    def run(self):
        while self.is_running:
            if self.connected is False:
                try:
                    server_address = (self.server_ip, self.server_port)
                    self.client_socket.connect(server_address)
                    self.client_socket.setblocking(False)
                    ready_to_read, _, _ = select.select((self.client_socket,), (), (), 10)
                    if len(ready_to_read) == 0:
                        self.connected = False
                        time.sleep(0.01)
                    else:
                        self.client_socket.recv(100)
                        data_out = bytes('Robot socket')
                        self.client_socket.sendall(data_out)
                        ready_to_read, _, _ = select.select((self.client_socket,), (), (), 10)
                        self.client_socket.recv(100)
                        if len(ready_to_read) == 0:
                            self.connected = False
                            time.sleep(0.01)
                        else:
                            self.connected = True
                            #print('connected')
                except (socket.error, ValueError):
                    self.connected = False
                    #print('Error')
                    time.sleep(0.01)
            else:
                data_in = b''
                try:
                    try:
                        ready_to_read, _, _ = select.select((self.client_socket,), (), (), 120)
                    except ValueError:
                        self.disconnect()
                        ready_to_read = []
                    if len(ready_to_read) == 0:
                        self.disconnect()
                    else:
                        while self.connected is True:
                            try:
                                data_t = self.client_socket.recv(25000)
                                data_in += data_t
                                if len(data_t) < 25000:
                                    break
                            except socket.error:
                                self.disconnect()
                                break

                        if len(data_in) == 0:
                            self.disconnect()

                        try:
                            msg = data_in.decode('utf-8')
                        except UnicodeDecodeError:
                            msg = ''

                        #print('============')
                        messages = msg.split('$%$')
                        for msg in messages:
                            if msg[0:5] == 'RESET':
                                SerialThread.message = "RESET"
                            elif msg[0:4] == 'STOP':
                                self.robot_velocity = 0
                                self.robot_angle = 0
                                self.robot_rotation = 0
                            elif msg[0:3] == 'VEL':
                                try:
                                    self.robot_velocity = int(msg[3:])
                                    #print('Velocity: {}'.format(self.robot_velocity))
                                except (ValueError, IndexError):
                                    pass
                            elif msg[0:5] == 'ANGLE':
                                try:
                                    self.robot_angle = int(msg[5:])
                                    #print('Angle: {}'.format(self.robot_angle))
                                except (ValueError, IndexError):
                                    pass
                            elif msg[0:3] == 'ROT':
                                try:
                                    self.robot_rotation = int(msg[3:])
                                    #print('Rotation: {}'.format(self.robot_rotation))
                                except (ValueError, IndexError):
                                    pass
                except socket.error:
                    self.disconnect()

    def disconnect(self):
        self.connected = False
        self.robot_velocity = 0
        self.robot_angle = 0
        self.robot_rotation = 0
        try:
            self.client_socket.close()
            self.client_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
            time.sleep(0.1)
        except socket.error:
            pass

    def send__data(self, string):
        # Funkcja dopisująca separator do stringa przesłanego z arduino i wysyłająca wiadomość na serwer
        data_out_str = '$%$' + string + '$%$'
        data_out = bytes(data_out_str)
        try:
            self.client_socket.sendall(data_out)
        except socket.error:
            self.connected = False


class SerialThread(Thread):
    def __init__(self, baud, usb):
        Thread.__init__(self)
        self.arduino_serial = serial.Serial(usb, baud, timeout=0.3)
        self.message = ""

    def run(self):
        while True:
            if self.message == "": self.message = "steering:" + str(conn.robot_velocity) + "/" + \
                                                  str(conn.robot_angle) + "*" + \
                                                  str(conn.robot_rotation) + "&\n"
            self.arduino_serial.write(self.message.encode('utf-8'))
            response = self.arduino_serial.readline().decode('utf-8').rstrip()
            czujniki = response.split("$%$")
            if len(czujniki) == 2:
                enk_data = czujniki[0]
                imu_data = czujniki[1]
                conn.send__data(enk_data)
                conn.send__data(imu_data)
            self.message = ""
            time.sleep(0.3)

conn = ClientThread(IP, PORT)
ser = SerialThread(BAUDRATE, USB_PORT)
conn.start()
ser.start()
