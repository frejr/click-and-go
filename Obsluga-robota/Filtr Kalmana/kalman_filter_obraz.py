# Wersja, gdzie filtr przystosowany jest do otrzymywania informacji o położeniu z wszystkich czujnikow
# w tym obrazu (bez dostosowanych macierzy kowariancji) - zakomentowane


"""
wektor stanu:
X = [
x
y
th
x*
y*
th*
x**
y**
]
gdzie th oznacza orientację robota, a * pochodną
"""


from pykalman import KalmanFilter
import numpy as np
import serial
import string
import math
import time
from time import time
import re


np.set_printoptions(linewidth=300)  # Do wyswietlania

n_timesteps = 50
n_dim_state = 8

dT = 0.02  # Krok czasowy

# transition_matrix - przy jej użyciu zachodzi estymacja na podstawie poprzedniego stanu
A = [[1, 0, 0, dT,   0,  0, (dT**2)/2,         0],
     [0, 1, 0,  0,  dT,  0,         0, (dT**2)/2],
     [0, 0, 1,  0,   0, dT,         0,         0],
     [0, 0, 0,  1,   0,  0,        dT,         0],
     [0, 0, 0,  0,   1,  0,         0,        dT],
     [0, 0, 0,  0,   0,  0,         0,         0],
     [0, 0, 0,  0,   0,  0,         0,         0],
     [0, 0, 0,  0,   0,  0,         0,         0]]


# observation_matrix - określa które zmienne stanu są mierzone
# Samo IMU (th', x'', y'')
H = [[0, 0, 0, 0, 0, 1, 0, 0],
     [0, 0, 0, 0, 0, 0, 1, 0],
     [0, 0, 0, 0, 0, 0, 0, 1]]

# Enkodery (tylko x' i y') + IMU + obraz. W kolejnosci: x, y, x', y', th', x'', y''
# H = [[1, 0, 0, 0, 0, 0, 0, 0],
#      [0, 1, 0, 0, 0, 0, 0, 0],
#      [0, 0, 0, 1, 0, 0, 0, 0],
#      [0, 0, 0, 0, 1, 0, 0, 0],
#      [0, 0, 0, 0, 0, 1, 0, 0],
#      [0, 0, 0, 0, 0, 0, 1, 0],
#      [0, 0, 0, 0, 0, 0, 0, 1]]


# transition_covariance - określa błąd estymacji (jest dodawana przy każdej estymacji)
Q = [[0.8, 0, 0, 0, 0, 0, 0, 0],
     [0, 0.8, 0, 0, 0, 0, 0, 0],
     [0, 0, 0.8, 0, 0, 0, 0, 0],
     [0, 0, 0, 0.8, 0, 0, 0, 0],
     [0, 0, 0, 0, 0.8, 0, 0, 0],
     [0, 0, 0, 0, 0, 3.8, 0, 0],
     [0, 0, 0, 0, 0, 0, 3.8, 0],
     [0, 0, 0, 0, 0, 0, 0, 3.8]]


# initial_state_covariance - określa początkowy błąd estymacji
P0 = [[0, 0, 0, 0, 0, 0, 0, 0],
      [0, 0, 0, 0, 0, 0, 0, 0],
      [0, 0, 0, 0, 0, 0, 0, 0],
      [0, 0, 0, 0, 0, 0, 0, 0],
      [0, 0, 0, 0, 0, 0, 0, 0],
      [0, 0, 0, 0, 0, 0, 0, 0],
      [0, 0, 0, 0, 0, 0, 0, 0],
      [0, 0, 0, 0, 0, 0, 0, 0]]


# initial_state_mean - wartosci poczatkowe wektora stanu
X0 = [0,
      0,
      0,
      0,
      0,
      0,
      0,
      0]

# observation_covariance - błąd pomiaru

# Samo IMU (th', x'', y'')
R = [[0, 0, 0],
     [0, 0, 0],
     [0, 0, 0]]

# Enkodery + IMU + obraz(niezerowe wartości tylko dla x, y, x' i y' bo tylko one są też estymowane)
# W kolejnosci: x', y', th', x'', y''
# R = [[0.7, 0,   0,   0,   0, 0, 0],
#      [0,   0.7, 0,   0,   0, 0, 0],
#      [0,   0,   0.7, 0,   0, 0, 0],
#      [0,   0,   0,   0.7, 0, 0, 0],
#      [0,   0,   0,   0,   0, 0, 0],
#      [0,   0,   0,   0,   0, 0, 0],
#      [0,   0,   0,   0,   0, 0, 0]]

# Stworzenie obiketu klasy KalmanFiltetr z zdefiniowanymi macierzami
kf = KalmanFilter(transition_matrices=A,
                  observation_matrices=H,
                  transition_covariance=Q,
                  observation_covariance=R,
                  initial_state_mean=X0,
                  initial_state_covariance=P0)

# Wektor stanu, aktualizowany w każdym kroku
filtered_state_means = np.zeros(n_dim_state)

# Macierz określająca błąd wyznaczonego stanu, aktualizowana w każdym kroku
filtered_state_covariances = np.zeros((n_dim_state, n_dim_state))



ser = serial.Serial(port='COM10', baudrate=115200, timeout=1) #Czytanie z portu szeregowego zostanie zamienione na
                                                              #odbieranie poprzez Wifi

zyroskop = 0
akcelerometr = [0, 0]
a_x = 0
a_y = 0
t = 0

# Inicjalizacja
filtered_state_means = X0
filtered_state_covariances = P0


# Petla czasowa, która stąd zniknie jak już wszystko połączymy na robocie
while(1):
    line = str(ser.readline())  # czytanie lini z Arduino do Pythona
    if line.find("!AN:") != -1:  # filtrowanie niekompletnych linii
        line = line.replace("!AN:", "")  # usuwanie "!ANG:"
        words = str.split(line, ",")  # dzielenie na wyrazy
        if len(words) > 2:
            try:
                zyroskop = math.radians(float(words[2]) / 14.4444)  # czytaj pomiar z zyroskopu
                akcelerometr[0] = float(words[3])/256.0  # czytaj pomiar z akcelerometru w osi X i rzutuj do m^2/s
                akcelerometr[1] = float(words[4])/256.0  # czytaj pomiar z akcelerometru w osi Y i rzutuj do m^2/s

                # Przyspieszenia wzdluz bazowych (poczatkowych) osi ukladu wspolrzednych
                # (obrot ukladu lokalnego przeciwnie do ruchu wskazowek zegara)
                a_x = (akcelerometr[0] * math.cos(filtered_state_means[2]) -
                       akcelerometr[1] * math.sin(filtered_state_means[2]))

                a_y = (akcelerometr[0] * math.sin(filtered_state_means[2]) +
                       akcelerometr[1] * math.cos(filtered_state_means[2]))



                # Uaktualnianie macierzy kowariancji oraz wektora stanu (przewidywanie i pomiary)
                # observation - pomiary w tej chwili czasu
                filtered_state_means, filtered_state_covariances = kf.filter_update(
                    filtered_state_means,
                    filtered_state_covariances,
                    observation=[zyroskop, a_x, a_y])



                #print(np.column_stack((['x', 'y', 'th', 'x*', 'y*', 'th*', 'x**', 'y**'], filtered_state_means)))
            except:
                print("Invalid line")








