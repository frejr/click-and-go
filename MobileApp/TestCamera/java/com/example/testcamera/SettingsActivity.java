package com.example.testcamera;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.widget.EditText;

public class SettingsActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings);

        Intent i = getIntent();
        String ipText = i.getStringExtra ("ip");
        String portText = i.getStringExtra ("port");

        EditText ipEdit = (EditText)findViewById(R.id.ipEditText);
        EditText portEdit = (EditText)findViewById(R.id.portEditText);

        ipEdit.setText (ipText);
        portEdit.setText(portText);
   }

    public void onBackPressed() {
        super.onBackPressed();
        SharedPreferences settings = getSharedPreferences("CaG", Context.MODE_PRIVATE);
        EditText ipEdit = (EditText)findViewById(R.id.ipEditText);
        EditText portEdit = (EditText)findViewById(R.id.portEditText);
        SharedPreferences.Editor editor = settings.edit();
        editor.putString("ip_address", ipEdit.getText().toString());
        editor.putString("port_number", portEdit.getText().toString());
        editor.apply();
        finish();
    }
}