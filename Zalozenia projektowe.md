# Click and Go



Celem naszego projektu jest stworzenie aplikacji na telefon, która używając kamery urządzenia mobilnego będzie umożliwiała zaznaczenie punktu docelowego, do którego po wyznaczonej trasie (omijając przeszkody)  ma się przemieścić robot.

#### Założenia projektowe:
 - Aplikacja będzie kompatybilna z wybranym urządzeniem mobilnym z systemem android
 - Aplikacja będzie wykorzystywała tylną kamerę urządzenia mobilnego
 - Aplikacja będzie zawierała proste GUI na urządzenie mobilne ułatwiające pracę z robotem, oraz odbieranie komunikatów dotyczących stanu pracy programu
 - Dane uzyskiwane z urządzenia mobilnego będą przesyłane na komputer, gdzie będzie wyznaczana trajektoria poruszania się robota do wybranego punktu, tak aby nie doszło do kolizji z przeszkodami
 - Robot będzie poruszał się po jednolitej powierzchni i wykrywał nieruchome przeszkody w postaci kartonów
 - Aplikacja będzie odporna na niewielkie zakłócenia oraz drobne zmiany orientacji urządzenia mobilnego
 - Aby program działał prawidłowo kamerę będzie należało zorientować pod kątem odchylenia od poziomu nie większym niż 30 stopni
 - Aplikacja będzie kontrolowała pracę robota, badając jego bieżące położenie 
Zbudowany robot posiada koła w trzech osiach napędzane niezależnie pracującymi silnikami
 - Aplikacja umożliwia śledzenie oraz kontrolę robota, gdy robot oraz punkt docelowy są widoczne na ekranie urządzenia mobilnego
 - Robot porusza się po obszarze objętym przez kamerę urządzenia mobilnego ( po uzgodnieniu z prowadzącym będzie to fragment sali wykładowej o wymiarach ok 4m x 3m )
 - Użyte rozwiązania oraz wstępne założenia mogą ulec niewielkim zmianom w zależności od pojawiających się w trakcie realizacji projektu błędów oraz nieprzewidzianych sytuacji

#### Realizacja projektu:
##### Milestone 1:
 - Utworzenie serwera pozwalającego na komunikację urządzenia mobilnego z komputerem
 - Przesyłanie obrazu live z kamery urządzenia mobilnego do komputera
 - Opracowanie prymitywnego GUI ułatwiającego obsługę kamery i komunikację z komputerem
 - Przygotowanie odpowiedniego datasetu do dalszych etapów pracy
 - Wykrywanie na obrazie robota oraz przeszkód
 - Utworzenie konstrukcji robota oraz ocena i przygotowanie układów niezbędnych do jego funkcjonowania
 - Komunikacja robota z komputerem
 - Wykorzystanie filtru Kalmana do sterowania robotem, robot dojeżdża do zadanego punktu bez uwzględniania przeszkód, przy znanej orientacji początkowej
 - Wybranie oraz dostosowanie sieci neuronowej, tak aby była zdolna do detekcji kartonów i robota

##### Milestone 2:
 - Możliwość zaznaczanie punktu docelowego na ekranie urządzenia mobilnego oraz przesyłanie tych danych do komputera
 - Mapowanie obrazu otrzymywanego z urządzenia mobilnego  na płaszczyznę 2D
 - Wykrywanie przeszkód i zbieranie informacji na temat położenia robota na obrazie live
 - Określenie początkowej orientacji robota
 - Wyznaczanie trasy robota na podstawie danych z telefonu, z uwzględnieniem wykrytych przeszkód.
 - Odbieranie i wyświetlanie  komunikatów z komputera  na urządzeniu mobilnym( np: nie wykryto robota, robot dojechał do celu)

##### Milestone 3:
 - Dopracowanie GUI oraz dostosowanie jego do praktycznego wykorzystania
 - Filtracja zakłóceń występujących podczas rzeczywistej transmisji obrazu
 - Zamknięcie projektu w jednej spójnej aplikacji - stworzenie instalatora
 - Poprawienie pracy robota, polegające na zwiększeniu odporności na zakłócenia i zniwelowanie negatywnego wpływu czynników zewnętrznych
 - Robot porusza się po wyznaczonej trasie nie kolidując z przeszkodami

