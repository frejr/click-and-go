# == Przeksztalcenie plaszczyzny do grafu i znajdywanie najkrotszej sciezki == 

import numpy as np
import networkx as nx
from networkx.generators.lattice import grid_2d_graph


def construct_graph(G, graph_size_x, graph_size_y, img, cell_size_x, cell_size_y, margin):
    """
    Zbudowanie graf dodanie jego wierzchołków  i krawędzi
    """
    # Graf pomocniczy - do wierzcholkow
    G_1 = grid_2d_graph(graph_size_x, graph_size_y)

    # Wierzcholki
    G.add_nodes_from(G_1.nodes())

    # Krawedzie prostopadłe
    G.add_edges_from([
                         ((x, y), (x + 1, y))
                         for x in range(graph_size_x - 1)
                         for y in range(graph_size_y)
                     ] + [
                         ((x, y), (x, y + 1))
                         for x in range(graph_size_x)
                         for y in range(graph_size_y - 1)
                     ], weight=1.0)

    # Krawedzie przekatne
    G.add_edges_from([
                         ((x, y), (x + 1, y + 1))
                         for x in range(graph_size_x - 1)
                         for y in range(graph_size_y - 1)
                     ] + [
                         ((x + 1, y), (x, y + 1))
                         for x in range(graph_size_x - 1)
                         for y in range(graph_size_y - 1)
                     ], weight=1.4)

    # Usuwanie przeszkod
    nodes_to_remove = [(x, y) for x in range(graph_size_x)
                        for y in range(graph_size_y)
                         if np.count_nonzero(img[y*cell_size_y:(y+1)*cell_size_y, x*cell_size_x:(x+1)*cell_size_x] == 0) > 0]

    # Usuwanie węzłów znajdujących sie w otoczeniu przeszkody - w ten sposob zapobiegamy zderzeniu robota z przeszkodą
    # margin jest wyznaczany na podstawie promienia (zmienna radius) z funkcji find_object
    # margin określa ile sąsiednich wezłów zostanie usuniętych
    for _ in range(margin):
        left_n = [(x[0]-1, x[1]) for x in nodes_to_remove if (x[0]-1, x[1]) not in nodes_to_remove]
        right_n = [(x[0]+1, x[1]) for x in nodes_to_remove if (x[0]+1, x[1]) not in nodes_to_remove]
        up_n = [(x[0], x[1]-1) for x in nodes_to_remove if (x[0], x[1]-1) not in nodes_to_remove]
        down_n = [(x[0], x[1]+1) for x in nodes_to_remove if (x[0], x[1]+1) not in nodes_to_remove]
        nodes_to_remove.extend(left_n)
        nodes_to_remove.extend(right_n)
        nodes_to_remove.extend(up_n)
        nodes_to_remove.extend(down_n)

    G.remove_nodes_from(nodes_to_remove)
    # Jesli pikseli zerowych (oznaczajacych przeszkode) jest wiecej niz 1 - usuwamy caly nod
    # i razem z nim jego krawedzie


def find_path(G, start_node, end_node):
    """
    Znalezienie najkrotszej sciezki od punktu startowego do koncowego na podanym grafie

    """
    # Heurystyka do algorytmu A* - odleglosc po lini prostej
    distance = lambda pos_1, pos_2: ((pos_2[0] - pos_1[0]) ** 2 + (pos_2[1] - pos_1[1]) ** 2) ** 0.5

    # Znajdywanie sciezki - A*
    path = nx.astar_path(G, start_node, end_node, heuristic=distance)
    return path
