# ---------------------------------------------------------
# Wyznaczenie poczatkowej orientacji robota na obrazie live
# ---------------------------------------------------------
import numpy as np
from math import cos, sin


def fit_line_and_get_y_values(x_arr, y_arr):
    """
    Dopasowanie linii metodą najmniejszych kwadratow na podstawie otrzymanych wspolrzednych
    :param x_arr: lista wspolrzednych x robota zebrana z poszczegolnych klatek
    :param y_arr: lista wspolrzednych y robota zebrana z poszczegolnych klatek
    :return: lista skladajaca sie z wartosci linii dopasowanej do otrzymanych wspolrzednych (y = mx + c)
    """
    x_arr = np.asarray(x_arr)
    A = np.vstack([x_arr, np.ones(len(x_arr))]).T
    m, c = np.linalg.lstsq(A, y_arr, rcond=None)[0]
    fitted_y = m * x_arr + c
    return fitted_y


def get_rotated_img(x_s, x_e, y_s, y_e, clockwise):
    """
    :param x_s: wspolrzedna x poczatkowego polozenia robota
    :param x_e: wspolrzedna y koncowego polozenia robota
    :param y_s: wspolrzedna y poczatkowego polozenia robota
    :param y_e: wspolrzedna y koncowego polozenia robota
    :param clockwise: bool - czy kat ma byc mierzony zgodnie czy przeciwnie do ruchu wskazowek zegara wzgledem osi x
    :return:
        ang: to kat odchylenia wyznaczonego kierunku ruchu wzgledem osi poziomej
    """
    d_y = y_e - y_s
    d_x = x_e - x_s

    if clockwise:
        # Wyznaczenie kata w zakresie 0 do 360 zgodnie z ruchem wskazowek zegara wzgledem osi x
        ang = np.degrees(np.arctan2(d_y, d_x)) % 360.0
    else:
        # Wyznaczenie kata w zakresie 0 do 360 przeciwnie do ruchu wskazowek zegara wzgledem osi x
        ang = 360 - np.degrees(np.arctan2(d_y, d_x)) % 360.0

    return ang


def determine_initial_orientation(p_x, p_y):
    """
    Wyznaczanie początkowej orientacji robota opierając się na jego ruchu początkowym.
    :param p_x: Lista współrzędnych x robota otrzymana z pierwszych stu klatek obrazu live
    :param p_y: Lista współrzędnych y robota otrzymana z pierwszych stu klatek obrazu live
    :return:
        x_end: Wspolrzedna x polozenia robota po zakonczeniu kalibracji
        y_end: Wspolrzedna y polozenia robota po zakonczeniu kalibracji
        angle: Kat odchylenia wyznaczonego wzgledem osi X
    """
    y_val = fit_line_and_get_y_values(p_x, p_y)
    x_start, y_start = p_x[0], p_y[0]
    x_end, y_end = p_x[-1], y_val[-1]
    angle = get_rotated_img(x_start, x_end, y_start, y_end, clockwise=False)

    # return x_start, y_start, angle
    return int(x_end), int(y_end), angle


def transform_coor(h, k, ang, pos):
    """
    Wyznaczanie pozycji w poczatkowym ukladzie wspolrzednych robota, z polozenia na obrazie
    :param h: przesuniecie ukladu wspolrzednych w osi X
    :param k: przesuniecie ukladu wspolrzednych w osi Y
    :param ang: kat o jaki jest obrocona oś X ukladu poczatkowego, wzgledem osi X obrazu (przciwnie do ruchu wskazowek
    zegara), w radianach
    :param pos: wspolrzedne w ukladzie obrazu
    :return: wspolrzedne poczatkowym ukladzie wspolrzednych robota

    Macierz wypadkowa to zlozenie translacji, lustrzanego odbicia (osi Y względem osi X) oraz obrotu:

    rot = np.array([[cos(ang), sin(ang), 0],
                    [-sin(ang), cos(ang), 0],
                    [0, 0, 1]])
    macierz obrotu (przeciwnie do ruchu wskazowek zegara) jest w takiej postaci, bo obrot w ukladzie obrazu
    przedstawia sie inaczej niz w zwyklym ukladzie - inna orientacja osi Y (w konsekwencji nastepuje zamiana znaku kata)

    flip = np.array([[1, 0, 0],
                     [0, -1, 0],
                     [0, 0, 1]])

    trans = np.array([[1, 0, -h],
                      [0, 1, -k],
                      [0, 0, 1]])

    transform_matrix = np.linalg.inv(rot @ flip) @ trans
    """

    transform_matrix = np.array([[cos(ang), -sin(ang), -h * cos(ang) + k * sin(ang)],
                                 [-sin(ang), -cos(ang), h * sin(ang) + k * cos(ang)],
                                 [0, 0, 1]])

    pos_vec = np.array([[pos[0]], [pos[1]], [1]])

    new_pos = transform_matrix @ pos_vec

    return new_pos[0, 0], new_pos[1, 0]
